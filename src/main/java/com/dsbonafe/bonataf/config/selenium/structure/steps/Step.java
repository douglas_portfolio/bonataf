package com.dsbonafe.bonataf.config.selenium.structure.steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class Step {
	protected static final Logger LOG = LoggerFactory.getLogger(Step.class);
}
