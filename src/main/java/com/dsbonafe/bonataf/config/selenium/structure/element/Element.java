package com.dsbonafe.bonataf.config.selenium.structure.element;

import java.util.Optional;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.dsbonafe.bonataf.config.selenium.driver.DriverNotDefinedException;
import com.dsbonafe.bonataf.config.selenium.structure.element.ByLocator.LocatorException;

import lombok.Getter;

@Component
@Scope(scopeName = "prototype")
public class Element {
	protected static final Logger LOG = LoggerFactory.getLogger(Element.class);
	private static int DEFAULT_TIMEOUT = 30;
	private static WebDriver driver;
	private WebElement element;

	@Getter
	private ByLocator locator;

	public Element(@Autowired WebDriver driver, ByLocator byLocator) {
		try {
			this.driver = driver;
			this.locator = byLocator;
		} catch (Exception e) {
			LOG.error("Error happening when instantiate element: ", e);
		}
	}

	public WebElement get() {
		Optional<WebElement> webElementOpt = Optional.empty();
		try {
			element = driver.findElement(locator.get());
			webElementOpt = Optional.ofNullable(element);
		} catch (LocatorException e) {
			LOG.error(e.getMessage(), e);
		}
		return webElementOpt.get();
	}

	public WebElement getWebElement() {
		if (null != this.element) {
			return this.element;
		} else
			return get();
	}

	public String getJavascriptGetElemetString() {
		String jsString = String.format("document.getElementBy%s(%s)", "%s", locator.getPath());
		switch (locator.getLocator()) {
		case CLASS_NAME:
			jsString = String.format(jsString, "ClassName");
			break;
		case ID:
			jsString = String.format(jsString, "Id");
			break;
		case NAME:
			jsString = String.format(jsString, "Name");
			break;
		case TAG_NAME:
			jsString = String.format(jsString, "TagName");
			break;
		default:
			jsString = "ERROR (Element.class): This type is not allowed.";
			break;
		}
		return jsString;
	}

	public void populateDropDown(SelectBy selectionMode, Object value) {
		if (!(value instanceof Integer) || !(value instanceof String)) {
			throw new ElementNotInteractableException("Selection value MUST be integer or string");
		}
		Select select = new Select(getWebElement());
		switch (selectionMode) {
		case INDEX:
			select.selectByIndex((Integer) value);
			break;
		case VALUE:
			select.selectByValue((String) value);
			break;
		case VISIBLE_TEXT:
			select.selectByVisibleText((String) value);
			break;

		default:
			select.selectByVisibleText((String) value);
			break;
		}
	}

	public Element waitElementCondidtion(ExpectedCondition condition) {
		waitElementCondidtion(condition, DEFAULT_TIMEOUT);
		return this;
	}

	public Element waitElementCondidtion(ExpectedCondition condition, int timeoutInSeconds) {
		WebDriverWait wait = new WebDriverWait(driver, Long.parseLong(String.valueOf(timeoutInSeconds)));
		wait.until(condition);
		return this;
	}

	public Element waitElementBeVisible(int timeout) {
		WebDriverWait wait = new WebDriverWait(driver, Long.parseLong(String.valueOf(timeout)));
		wait.until(ExpectedConditions.visibilityOf(getWebElement()));
		return this;
	}

	public void sendKeys(CharSequence text) {
		try {
			sendKeysSelenium(text);
		} catch (Exception e) {
			LOG.warn("Fail executing selenium SENDKEYS. Trying via JS", e);
			jsSendKeys(text);
		}
	}

	private void sendKeysSelenium(CharSequence text) {
		WebElement input = getWebElement();
		input.clear();
		input.sendKeys(text);
	}

	private void jsSendKeys(CharSequence text) {
		executeJavascript(String.format("%s.value='%s'", getJavascriptGetElemetString(), text));
	}

	public void click() {
		getWebElement().click();
	}

	public void clickExpectingCondition(ExpectedCondition condition) {
		click();
		waitElementCondidtion(condition);
	}

	public void jsClick() {
		try {
			waitElmentBeVisible();
			executeJavascript(String.format("arguments[0].click()", get()));
		} catch (Exception e) {
			executeJavascript(String.format("%s.click()", getJavascriptGetElemetString()));
		}
	}

	public void waitElementBeClickable() {
		waitElementCondidtion(ExpectedConditions.elementToBeClickable(getWebElement()));
	}

	public void waitElmentBeVisible() {
		waitElementCondidtion(ExpectedConditions.visibilityOf(getWebElement()));
	}

	public static void executeJavascript(String script) {
		((JavascriptExecutor) driver).executeScript(script);
	}

	public static void executeJavascript(String script, String arguments) {
		((JavascriptExecutor) driver).executeScript(script, arguments);
	}

	public enum SelectBy {
		INDEX, VALUE, VISIBLE_TEXT
	}
}
