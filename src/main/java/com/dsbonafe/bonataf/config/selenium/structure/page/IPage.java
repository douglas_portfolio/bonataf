package com.dsbonafe.bonataf.config.selenium.structure.page;

public interface IPage {

	public boolean isLoaded();
}
