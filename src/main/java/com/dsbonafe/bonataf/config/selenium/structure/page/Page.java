package com.dsbonafe.bonataf.config.selenium.structure.page;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.TargetLocator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.dsbonafe.bonataf.config.selenium.VideoRecorder;
import com.dsbonafe.bonataf.config.selenium.driver.DriverFactory;
import com.dsbonafe.bonataf.config.selenium.driver.DriverNotDefinedException;
import com.dsbonafe.bonataf.config.selenium.structure.element.ByLocator;
import com.dsbonafe.bonataf.config.selenium.structure.element.Element;
import com.dsbonafe.bonataf.config.selenium.structure.element.Locator;

@Component
public abstract class Page implements IPage {
	protected static final Logger LOG = LoggerFactory.getLogger(Page.class);

	@Value("${screenshotFolderPath}")
	private String screenshotFolderPath;

	protected WebDriver driver;

	public Page(@Autowired DriverFactory driverFactory) {
		try {
			driver = driverFactory.getDriver();
		} catch (DriverNotDefinedException e) {
			LOG.error("Page could not get driver from DriverFactory. Driver factory could not instantiate the driver.",
					e);
			;
		}
	}

	public WebDriver switchToNewWindow(String window) {
		return driver.switchTo().window(window);
	}

	public String captureScreenshot(String folder, String fileName) {
		if (null == screenshotFolderPath || screenshotFolderPath.isEmpty()) {
			return "YOU MUST SET screenshotFolderPath VARIABLE IN general.properties FILE.";
		}

		File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		File targetFile = new File(folder, fileName + ".png");

		try {
			FileUtils.copyFile(screenshotFile, targetFile);
		} catch (IOException e) {
			LOG.error("Error while writing SCREENSHOT file ", e);
		}

		return targetFile.getAbsolutePath();
	}

	protected Element find(Locator locator, String path, int timeout) {
		WebDriverWait webDriverWait = new WebDriverWait(driver, timeout);
		return new Element(driver, new ByLocator(locator, path)).waitElementBeVisible(timeout);
	}

	protected static void executeJavascript(String script) {
		Element.executeJavascript(script);
	}

	protected static void executeJavascript(String script, String args) {
		Element.executeJavascript(script, args);
	}

	protected void confirmAlertIfVisible() {
		if (isAlertVisible()) {
			confirmAlert();
		}
	}

	protected void confirmAlert() {
		String mainWindow = driver.getWindowHandle();
		driver.switchTo().alert().accept();
		driver.switchTo().window(mainWindow);
	}

	protected void dismissAlert() {
		String mainWindow = driver.getWindowHandle();
		driver.switchTo().alert().dismiss();
		driver.switchTo().window(mainWindow);
	}

	protected String getAlertMessage() {
		return driver.switchTo().alert().getText();
	}

	protected void sendKeysToAlert(String keys) {
		driver.switchTo().alert().sendKeys(keys);
	}

	protected TargetLocator switchTo() {
		return driver.switchTo();
	}

	protected Set<String> getWindowHandled() {
		return driver.getWindowHandles();
	}

	protected String getThisWindow() {
		return driver.getWindowHandle();
	}

	protected void recordVideo(@Autowired VideoRecorder recorder, String videoName) {
		recorder.startRecording(videoName);
	}

	protected void stopVideo(@Autowired VideoRecorder recorder) {
		recorder.stopRecording();
	}

	protected boolean isAlertVisible() {
		boolean response = false;
		try {
			String thisWindow = getThisWindow();
			switchTo().alert();
			switchToNewWindow(thisWindow);
			response = true;
		} catch (NoAlertPresentException e) {
			LOG.info("Alert was not showed.");
		}
		return response;
	}

}
