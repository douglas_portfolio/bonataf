package com.dsbonafe.bonataf.config.selenium.structure.element;

public enum Locator {
	CLASS_NAME, CSS, ID, LINKTEXT, NAME, PARTIAL_LINK_TEXT, TAG_NAME, XPATH;
}
