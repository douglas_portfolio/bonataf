package com.dsbonafe.bonataf.config.selenium.driver;

public class DriverNotDefinedException extends Exception {

	private static final long serialVersionUID = 2073972843590166965L;

	public DriverNotDefinedException(String message){
		super(message);
	}
	
	public DriverNotDefinedException(){};
}
