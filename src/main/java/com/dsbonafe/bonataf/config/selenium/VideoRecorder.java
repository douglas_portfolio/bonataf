package com.dsbonafe.bonataf.config.selenium;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.monte.media.Format;
import org.monte.media.FormatKeys;
import org.monte.media.FormatKeys.MediaType;
import org.monte.media.Registry;
import org.monte.media.VideoFormatKeys;
import org.monte.media.math.Rational;
import org.monte.screenrecorder.ScreenRecorder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.dsbonafe.bonataf.config.selenium.driver.DriverFactory;

import lombok.Getter;

@Component
public class VideoRecorder {

	protected static final Logger LOG = LoggerFactory.getLogger(DriverFactory.class);
	
	public VideoRecorder() {
		LOG.info("I intend to record the screen. Be advised.");
	}

	@Getter
	@Value("${videoRecordFolder}")
	String videoRecordFolder;
	private SpecializedScreenRecorder screenRecorder;

	public void startRecording(String videoName) {
		File file = new File(videoRecordFolder);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Rectangle captureSize = new Rectangle(0, 0, screenSize.width, screenSize.height);
		GraphicsConfiguration graphicsConfiguration = GraphicsEnvironment.getLocalGraphicsEnvironment()
				.getDefaultScreenDevice().getDefaultConfiguration();

		try {
			this.screenRecorder = new SpecializedScreenRecorder(graphicsConfiguration, captureSize,
					SpecializedFormatter.getDefault(), file, videoName);
			this.screenRecorder.start();
		} catch (IOException | AWTException e) {
			LOG.error(
					"Could not find folder where to save the video that was defined in bdd.properties as videoResultRecord property.",
					e);
		}
	}

	public void stopRecording() {
		try {
			if (null != screenRecorder) {
				screenRecorder.stop();
				LOG.info(String.format("Video recorded in the file: <%s/%s>.", videoRecordFolder,
						screenRecorder.getName()));
			} else {
				LOG.info("Video was not recorded.");
			}
		} catch (IOException e) {
			LOG.error("Error to find file to report. Could not stop recording.");
		}
	}

	private class SpecializedScreenRecorder extends ScreenRecorder {
		@Getter
		private String name;

		public SpecializedScreenRecorder(GraphicsConfiguration cfg, Rectangle captureArea,
				SpecializedFormatter formatter, File movieFolder, String name) throws IOException, AWTException {
			super(cfg, captureArea, formatter.fileFormat, formatter.screenFormat, formatter.mouseFormat,
					formatter.audioFormat, movieFolder);
			this.name = name;
		}

		@Override
		protected File createMovieFile(Format fileFormat) throws IOException {
			if (!movieFolder.exists()) {
				movieFolder.mkdirs();
			} else if (!movieFolder.isDirectory()) {
				throw new IOException("\"" + movieFolder + "\" is not a directory.");
			}

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");

			return new File(movieFolder,
					name + "-" + dateFormat.format(new Date()) + "." + Registry.getInstance().getExtension(fileFormat));
		}
	}

	@Getter
	public static class SpecializedFormatter {

		private Format fileFormat;
		private Format screenFormat;
		private Format mouseFormat;
		private Format audioFormat;

		public SpecializedFormatter(Format fileFormat, Format screenFormat, Format mouseFormat, Format audioFormat) {
			this.fileFormat = fileFormat;
			this.screenFormat = screenFormat;
			this.mouseFormat = mouseFormat;
			this.audioFormat = audioFormat;
		}

		public static SpecializedFormatter getDefault() {
			Format file = new Format(FormatKeys.MediaTypeKey, MediaType.FILE, FormatKeys.MimeTypeKey,
					FormatKeys.MIME_AVI);
			Format screen = new Format(FormatKeys.MediaTypeKey, MediaType.VIDEO, FormatKeys.EncodingKey,
					VideoFormatKeys.ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE, VideoFormatKeys.CompressorNameKey,
					VideoFormatKeys.ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE, VideoFormatKeys.DepthKey, 24,
					VideoFormatKeys.FrameRateKey, Rational.valueOf(15), VideoFormatKeys.QualityKey, 1.0F,
					VideoFormatKeys.KeyFrameIntervalKey, 15 * 60);
			Format mouse = new Format(FormatKeys.MediaTypeKey, MediaType.VIDEO, FormatKeys.EncodingKey, "black",
					VideoFormatKeys.FrameRateKey, Rational.valueOf(30));

			return new SpecializedFormatter(file, screen, mouse, null);

		}

	}
}
