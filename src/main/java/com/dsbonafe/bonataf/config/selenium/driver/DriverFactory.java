package com.dsbonafe.bonataf.config.selenium.driver;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.dsbonafe.bonataf.config.utils.MethodUtils;

@Component
public class DriverFactory {
	protected static final Logger LOG = LoggerFactory.getLogger(DriverFactory.class);

	@Value("${driversPath}")
	protected String driversPath;

	@Value("${browserName}")
	private String browserName;

	private static HashMap<String, String> capabilitiesMap;
	private static List<String> driverExtensionsPathList;
	private static HashMap<String, String> proxyPropertiesMap;
	private static List<String> browserArguments;

	private static WebDriver DRIVER;

	private static ChromeOptions chromeOptions = new ChromeOptions();

	public DriverFactory() {
		getCapabilitiesFromBrowserPropertiesFile();
		getExtensionsPathListFromBrowserExentensionPathListFile();
		getProxyPropertiesFromBrowserProxyPropertiesFile();
		getBrowserArgumentsFromBrowserArgumentsFile();
	}

	public WebDriver getDriver() throws DriverNotDefinedException {
		if (null == DRIVER) {
			selectDriver(browserName.toLowerCase());
		}
		return DRIVER;
	}

	private void selectDriver(String browser) throws DriverNotDefinedException {
		switch (browser) {
		case "chrome":
			DRIVER = getChromeDriver();
			break;

		case "firefox":
			DRIVER = getFirefoxDriver();
			break;

		default:
			DRIVER = getFirefoxDriver();
			break;
		}
	}

	private static WebDriver getFirefoxDriver() {
		return new FirefoxDriver();
	}

	private WebDriver getChromeDriver() throws DriverNotDefinedException {
		Optional<WebDriver> driverOpt = getOptionalChromeDriver();
		if (driverOpt.isPresent()) {
			return driverOpt.get();
		}
		throw new DriverNotDefinedException("WebDriver was not successfully defined in factory.");
	}

	private Optional<WebDriver> getOptionalChromeDriver() {
		if (null != driverExtensionsPathList && !driverExtensionsPathList.isEmpty()) {
			for (String extPath : driverExtensionsPathList) {
				chromeOptions.addExtensions(new File(extPath));
			}
		}
		setProxyIfConfigured();
		setArgumentsIfConfigurated();
		System.setProperty("webdriver.chrome.driver", String.format("%s\\chromedriver.exe", driversPath));   
		WebDriver driver = new ChromeDriver(chromeOptions);
		return Optional.ofNullable(driver);
	}

	private static void setArgumentsIfConfigurated() {
		chromeOptions.addArguments(browserArguments);
	}

	private static void setProxyIfConfigured() {
		if (null != proxyPropertiesMap && !proxyPropertiesMap.isEmpty()) {
			Proxy proxy = new Proxy();
			String host = proxyPropertiesMap.get("host");
			String port = proxyPropertiesMap.get("port");
			proxy.setHttpProxy(String.format("%s:%s", host, port));
			// You could include other convenient properties here.
			// This is very useful for security automation testing.
			chromeOptions.setProxy(proxy);
		}
	}

	private static void getProxyPropertiesFromBrowserProxyPropertiesFile() {
		HashMap<String, String> proxyProps = MethodUtils.readKeyValuePropertiesFile("selenium/proxyDefinition").get();
		if (!proxyProps.isEmpty()) {
			proxyPropertiesMap = new HashMap<>(proxyProps);
		}
	}

	private static void getExtensionsPathListFromBrowserExentensionPathListFile() {
		List<String> extensionPaths = MethodUtils.readPropertiesListFromFile("selenium/browserExtensionPathList").get();
		if (!extensionPaths.isEmpty()) {
			driverExtensionsPathList = new ArrayList<>(extensionPaths);
		}
	}

	private static void getCapabilitiesFromBrowserPropertiesFile() {
		HashMap<String, String> capabilities = MethodUtils.readKeyValuePropertiesFile("selenium/browserCapabilities")
				.get();
		if (!capabilities.isEmpty()) {
			capabilitiesMap = new HashMap<>(capabilities);
		}
	}

	private static void getBrowserArgumentsFromBrowserArgumentsFile() {
		List<String> arguments = MethodUtils.readPropertiesListFromFile("selenium/browserArguments").get();
		if (!arguments.isEmpty()) {
			browserArguments = new ArrayList<>(arguments);
		}
	}

}
