package com.dsbonafe.bonataf.config.selenium.structure;

import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.BeforeStories;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.dsbonafe.bonataf.config.selenium.VideoRecorder;
import com.dsbonafe.bonataf.config.selenium.driver.DriverFactory;
import com.dsbonafe.bonataf.config.selenium.driver.DriverNotDefinedException;
import com.dsbonafe.bonataf.config.selenium.structure.page.Page;

@Component
public class Lifecycle {
	protected static final Logger LOG = LoggerFactory.getLogger(Lifecycle.class);
	
	private WebDriver driver;
	private VideoRecorder recorder;

	public Lifecycle(@Autowired DriverFactory factory, @Autowired VideoRecorder recorder) throws DriverNotDefinedException {
		this.driver = factory.getDriver();
		this.recorder = recorder;
	}

	@BeforeStories
	public void beforeStories() {
		LOG.info("WELCOME TO BONATAF!!!");
		recorder.startRecording("AutomationRecorder");
	}

	@AfterStories
	public void afterStories() {
		if (null != driver) {
			driver.close();
		}
		recorder.stopRecording();
	}

}
