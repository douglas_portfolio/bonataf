package com.dsbonafe.bonataf.config.selenium;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.springframework.stereotype.Component;

@Component
public class StepsContext {
	
	private Stack<String> contextStack = new Stack<>();
	private Map<String, Object> contextMap = new HashMap<>();
	
	public void pushToContext(String data){
		contextStack.push(data);
	}
	
	public String popFromContext(){
		return contextStack.pop();
	}
	
	public void addToContextMap(String key, Object data){
		contextMap.put(key, data);
	}
	
	public Object getFromContextMap(String key){
		return contextMap.get(key);
	}

}
