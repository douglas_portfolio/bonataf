package com.dsbonafe.bonataf.config.selenium.structure.element;

import org.openqa.selenium.By;

import lombok.Getter;

public class ByLocator {

	@Getter
	private Locator locator;
	@Getter
	private String path;

	public ByLocator(Locator locator, String path) {
		this.locator = locator;
		this.path = path;
	}

	public By get() throws LocatorException {
		switch (locator) {
		case CLASS_NAME:
			return By.className(path);

		case CSS:
			return By.cssSelector(path);

		case ID:
			return By.id(path);
			
		case LINKTEXT:
			return By.linkText(path);
			
		case NAME:
			return By.name(path);
			
		case PARTIAL_LINK_TEXT:
			return By.partialLinkText(path);
			
		case TAG_NAME:
			return By.tagName(path);
			
		case XPATH:
			return By.xpath(path);
		default:
			throw new LocatorException("Invalid locator");
		}
	}
	
	public class LocatorException extends Exception{
		private static final long serialVersionUID = -8597591000727410723L;

		public LocatorException() {
		}
		
		public LocatorException(String message){
			super(message);
		}
	}
}
