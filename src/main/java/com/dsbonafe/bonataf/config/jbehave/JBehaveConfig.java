package com.dsbonafe.bonataf.config.jbehave;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.embedder.UnmodifiableEmbedderControls;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.spring.SpringStepsFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

public abstract class JBehaveConfig extends JUnitStories {

	private static final int STORY_TIMEOUT_SECS = 3600;
	private final Logger LOG = (Logger) LoggerFactory.getLogger(JBehaveConfig.class);

	private ApplicationContext springContext;
	private ApplicationContext applicationContext;

	// Embedder configuration variables

	private boolean ignoreFailureInStories = true;
	private boolean failOnStoryTimeout = false;
	private boolean generateViewAfterStories = false;
	private boolean ignoreFailureInView = false;
	private boolean verboseFailures;

	// Class Interface Methods

	public JBehaveConfig() {
		// Will retrieve the spring context instance from the child classes
		// getAnnotatedContext() overriden method
		springContext = getContextInstance();
		if (springContext != null) {
			configuredEmbedder();
		}
	}

	@Override
	public Configuration configuration() {
		return new MostUsefulConfiguration()
				// where to find stories
				.useStoryLoader(new LoadFromClasspath(this.getClass()))
				// CONSOLE, HTML & TXT reporting
				.useStoryReporterBuilder(new StoryReporterBuilder().withDefaultFormats().withFormats(Format.CONSOLE,
						Format.HTML, Format.TXT));
	}
		
	@Override
	public Embedder configuredEmbedder() {
		springContext = getContextInstance();
		Embedder embedder = super.configuredEmbedder();
		if (!(embedder.embedderControls() instanceof UnmodifiableEmbedderControls)) {
			return configureEmbederControls(embedder);
		}
		return embedder;
	}

	@Override
	public InjectableStepsFactory stepsFactory() {
		return new SpringStepsFactory(configuration(), springContext);
	}

	// Scope-class Methods

	private Embedder configureEmbederControls(Embedder embedder) {
		embedder.embedderControls().doIgnoreFailureInStories(ignoreFailureInStories)
				.useStoryTimeouts(Long.toString(STORY_TIMEOUT_SECS)).doFailOnStoryTimeout(failOnStoryTimeout)
				.doGenerateViewAfterStories(generateViewAfterStories).doIgnoreFailureInView(ignoreFailureInView)
				.doVerboseFailures(verboseFailures);
		return embedder;
	}

	protected ApplicationContext getContextInstance() {
		if (applicationContext == null) {
			applicationContext = getAnnotatedApplicationContext();
		}
		return applicationContext;
	}



	// Abstract Metods

	/**
	 * This method must be overridden by child classes. It will use the
	 * configuration class of the test project to provide
	 * AnnotadedApplicationContext as per the spring framework class.
	 * 
	 * @return ApplicationContext
	 * @see org.springframework.context.ApplicationContext
	 */
	protected abstract ApplicationContext getAnnotatedApplicationContext();

}
