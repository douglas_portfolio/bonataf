package com.dsbonafe.bonataf.config.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class MethodUtils {

	protected static final Logger LOG = LoggerFactory.getLogger(MethodUtils.class);

	public static Optional<List<String>> readPropertiesListFromFile(String resource) {
		List<String> propertiesList = new ArrayList<>();
		String filePath = fromResource(resource).getPath();
		try {
			Files.lines(new File(filePath).toPath()).forEach(l -> propertiesList.add(l));
		} catch (IOException e) {
			LOG.error(String.format("Could not find file %s", filePath));
		}
		return Optional.ofNullable(propertiesList);
	}

	public static Optional<HashMap<String, String>> readKeyValuePropertiesFile(String resource) {
		Properties prop = new Properties();
		HashMap<String, String> propMap = new HashMap<>();
		try (InputStream input = fromResourceAsStream(resource)){
			prop.load(input);
			while (prop.keys().hasMoreElements()) {
				String key = prop.keys().nextElement().toString();
				propMap.put(key, prop.getProperty(key));
			}
		} catch (IOException ex) {
			LOG.error("Could not find property file.", ex);
		}
		return Optional.ofNullable(propMap);
	}

	private static InputStream fromResourceAsStream(String folderAndFile) {
		return MethodUtils.class.getClassLoader().getResourceAsStream(String.format("properties/%s.properties", folderAndFile));
	}
	
	private static URL fromResource(String folderAndFile){
		return MethodUtils.class.getClassLoader().getResource(String.format("properties/%s.properties", folderAndFile));
	}
}
