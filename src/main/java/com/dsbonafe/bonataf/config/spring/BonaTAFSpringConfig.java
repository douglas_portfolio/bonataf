package com.dsbonafe.bonataf.config.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@PropertySources({ 
	@PropertySource("properties/selenium/browserArguments.properties"),
	@PropertySource("properties/selenium/browserCapabilities.properties"),
	@PropertySource("properties/selenium/browserExtensionPathList.properties"),
	@PropertySource("properties/selenium/proxyDefinition.properties"),
	@PropertySource("properties/selenium/general.properties"),
	@PropertySource("properties/bdd.properties")
})
@ComponentScan({ "com.dsbonafe" })
public class BonaTAFSpringConfig {

}
