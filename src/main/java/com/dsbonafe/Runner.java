package com.dsbonafe;

import java.net.URL;
import java.util.Arrays;
import java.util.List;

import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.StoryFinder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.dsbonafe.bonataf.config.jbehave.JBehaveConfig;
import com.dsbonafe.bonataf.config.spring.BonaTAFSpringConfig;

public class Runner extends JBehaveConfig {

	private static final String STORY_NAME = "*";
	private static final String EXCLUDING_STORIES = "";
	private static final String STORY_PATHS_PROP = "storyPaths";
	private static final String PATHS_BREAKER = ";";

	// Main method

	@Override
	public List<String> storyPaths() {
		String storyPaths = System.getProperty(STORY_PATHS_PROP);

		if (storyPaths == null) {
			return new StoryFinder().findPaths(searchIn(), includes(), excludes());
		}

		return Arrays.asList(storyPaths.split(PATHS_BREAKER));
	}

	@Override
	protected ApplicationContext getAnnotatedApplicationContext() {
		return new AnnotationConfigApplicationContext(BonaTAFSpringConfig.class);
	}

	// Used methods

	private final URL searchIn() {
		return CodeLocations.codeLocationFromClass(this.getClass());
	}

	private final String includes() {
		return String.format("stories/**/%s.story", STORY_NAME.trim().toLowerCase());
	}

	private final String excludes() {
		return EXCLUDING_STORIES;
	}
	
	public static void main(String[] args) throws Throwable {
		new Runner().run();
	}

}
