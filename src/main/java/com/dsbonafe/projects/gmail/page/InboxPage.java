package com.dsbonafe.projects.gmail.page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Component;

import com.dsbonafe.bonataf.config.selenium.driver.DriverFactory;
import com.dsbonafe.bonataf.config.selenium.structure.element.Element;
import com.dsbonafe.bonataf.config.selenium.structure.element.Locator;
import com.dsbonafe.bonataf.config.selenium.structure.page.Page;

@Component
public class InboxPage extends Page {

	public InboxPage(DriverFactory driverFactory) throws Exception {
		super(driverFactory);
	}

	private static final String TITLE_XPATH = "//title[contains(text(), '%s')]";
	private static final String COMPOSE_XPATH = "//*[contains(text(), 'Compose')]";
	private static final String TEXT_AREA_TO_BEFORE = "//div[contains(@id, ':18h')]";
	private static final String TEXT_AREA_TO = "//textarea[contains(@aria-label, 'To')]";
	private static final String TEXT_AREA_BODY = "//div[@aria-label='Message Body']";
	private static final String CC_BTN = "//span[contains(@data-tooltip,'Cc')]";
	private static final String TEXT_AREA_CC = ".//textarea[contains(@aria-label, 'Cc')]";
	private static final String MAIL_BODY_SETTINGS = "(//div[contains(@class, 'J-J5-Ji J-JN-M-I-JG')])[last()]";
	private static final String LABEL_SETTING = "(//div[contains(text(), 'Label')])";
	private static final String SEARCH_LABEL_INPUT = "//div[contains(@class,'A0')]//ancestor::div//input";
	private static final String SOCIAL_TAB_PATH = "(//div[contains(text(), 'Social')])[1]";
	private static final String EMAILS_LIST_ITEM = "//span[contains(@class, 'bog')]//span";

	private String foundEmailPath;

	@Override
	public boolean isLoaded() {
		boolean response = true;
		try {
			find(Locator.XPATH, COMPOSE_XPATH, 10);
		} catch (Exception e) {
			LOG.error("Inbox page was not loaded.", e);
			response = false;
		}
		return response;
	}

	public InboxPage clickOnCompose() {
		try {
			find(Locator.XPATH, COMPOSE_XPATH, 10).click();
		} catch (Exception e) {
			LOG.error(String.format("Couldn't find compose button: %s", COMPOSE_XPATH));
		}
		return this;
	}

	public InboxPage sendEmailSubject(String emailSubject) {
		try {
			find(Locator.NAME, "subjectbox", 10).sendKeys(emailSubject);
		} catch (Exception e) {
			LOG.error("Error while writing subject.", e);
		}
		return this;
	}

	public InboxPage sendEmailBody(String emailBody) {
		try {
			find(Locator.XPATH, TEXT_AREA_BODY, 10).sendKeys(emailBody);
		} catch (Exception e) {
			LOG.error("Error during writing body.", e);
		}
		return this;
	}

	public InboxPage sendTo(String to) {
		try {
			find(Locator.XPATH, TEXT_AREA_TO_BEFORE, 10).click();
			find(Locator.XPATH, TEXT_AREA_TO, 10).sendKeys(to);
		} catch (Exception e) {
			LOG.error("Error during writing email to.", e);
		}
		return this;
	}

	public void clickOnSend() {
		try {
			find(Locator.XPATH, "//div[contains(text(), 'Send')]", 10).click();
		} catch (Exception e) {
			LOG.error("Error during writing email to.", e);
		}
	}

	public InboxPage clickOnMailSettings() {
		try {
			find(Locator.XPATH, MAIL_BODY_SETTINGS, 10).click();
		} catch (Exception e) {
			LOG.error(String.format("Could not click on MAIL SETTINGS with path <%s>.", MAIL_BODY_SETTINGS), e);
		}
		return this;
	}

	public InboxPage clickOnSettingLabel() {
		try {
			find(Locator.XPATH, LABEL_SETTING, 10).click();
		} catch (Exception e) {
			LOG.error(String.format("Could not click on LABEL SETTING with path <%s>.", LABEL_SETTING), e);
		}
		return this;
	}

	public InboxPage searchForLabel(String label) {
		try {
			Element searchLabelInput = find(Locator.XPATH, this.SEARCH_LABEL_INPUT, 10);
			searchLabelInput.click();
			searchLabelInput.sendKeys(label);
			searchLabelInput.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			LOG.error(String.format("Could not send search for label on path <%s>.", MAIL_BODY_SETTINGS), e);
		}
		return this;
	}

	public InboxPage sendCc(String cc) {
		try {
			find(Locator.XPATH, CC_BTN, 10).click();
			Element toField = find(Locator.XPATH, TEXT_AREA_CC, 10);
			toField.click();
			toField.sendKeys(cc);
		} catch (Exception e) {
			LOG.error("Error during writing email to.", e);
		}
		return this;
	}

	public boolean isOnAccount(String email) {
		boolean response = true;
		try {
			find(Locator.XPATH, "//span[contains(@class, 'gb_9a gbii')]", 10).click();
			find(Locator.XPATH, "//div[contains(@class, 'gb_Eb')]", 10).getWebElement().getText().contains(email);
		} catch (Exception e) {
			response = false;
		}
		return response;
	}

	public InboxPage goToTab(String tabName) {
		try {
			find(Locator.XPATH, SOCIAL_TAB_PATH, 10).click();
		} catch (Exception e) {
			LOG.error("Failed to click in SOCIAL tab.", e);
		}
		return this;
	}

	public InboxPage searchEmail(String emailSubject) {
		String searchLine = "\"label:%s\", \"subject:%s\"";
		searchLine = String.format(searchLine, "Social", emailSubject);
		try {
			Element searchBar = find(Locator.CLASS_NAME, "gb_cf", 10);
			searchBar.click();
			searchBar.getWebElement().clear();
			searchBar.sendKeys("xover");
			searchBar.getWebElement().submit();
			confirmAlertIfVisible();
		} catch (Exception e) {
			LOG.error("Failed to fill search bar.", e);
		}
		return this;
	}

	public boolean isEmailPresent(String emailSubject) {
		boolean response = false;
		int i = 0;
		try {
			List<WebElement> emails = driver.findElements(By.xpath(EMAILS_LIST_ITEM));
			for (WebElement mailSubject : emails) {
				i++;
				if (mailSubject.getText().contains(emailSubject)) {
					response = true;
					foundEmailPath = String.format("(%s)[%d]", EMAILS_LIST_ITEM, i);
					break;
				}
			}
		} catch (Exception e) {
			response = false;
		}
		return response;
	}

	public void openEmail() {
		try {
			find(Locator.XPATH, foundEmailPath, 10).click();
		} catch (Exception e) {
			LOG.error(String.format("Could not clik on email with path <%s>", foundEmailPath));
		}
	}

	public String getContentOfBody() {
		String response;
		try {
			response = find(Locator.XPATH, "//div[contains(@class, 'ii gt')]", 10).getWebElement().getText();
		} catch (Exception e) {
			response = e.getMessage();
			LOG.error("Could not get the mail content.", response, e);
		}
		return response;
	}

	public InboxPage starUnstarEmail() {
		try {
			find(Locator.XPATH, "(//span[contains(@title, 'tarred')])[1]", 10).click();
		} catch (Exception e) {
			LOG.error("Could not star email.", e);
		}
		return this;
	}
}
