package com.dsbonafe.projects.gmail.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dsbonafe.bonataf.config.selenium.driver.DriverFactory;
import com.dsbonafe.bonataf.config.selenium.structure.element.Element;
import com.dsbonafe.bonataf.config.selenium.structure.element.Locator;
import com.dsbonafe.bonataf.config.selenium.structure.page.Page;

@Component
public class GmailPage extends Page {

	private static final String LOGIN_FIELD = "(//input)[1]";
	private static final String INBOX_LINK = "//a[contains(text(), 'Inbox')]";

	public GmailPage(@Autowired DriverFactory driverFactory) {
		super(driverFactory);
	}

	public boolean isLoaded() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			Element loginFieldElement = find(Locator.XPATH, LOGIN_FIELD, 10);
			wait.until(ExpectedConditions.visibilityOf(loginFieldElement.getWebElement()));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void go() {
		driver.get("http://www.gmail.com");
	}

	public void login() {
		try {
			find(Locator.XPATH, LOGIN_FIELD, 10).click();
		} catch (Exception e) {
			LOG.error("Login method error.", e);
		}
	}

	public GmailPage sendLogin(String username) {
		try {
			find(Locator.XPATH, LOGIN_FIELD, 10).sendKeys(username);
		} catch (Exception e) {
			LOG.error(String.format("Could not send login text to the field <%s>", LOGIN_FIELD), e);
		}
		return this;
	}

	public GmailPage clickOnNextLogin() {
		try {
			ExpectedCondition<WebElement> pageChanged = ExpectedConditions
					.visibilityOfElementLocated(By.name("password"));
			find(Locator.ID, "identifierNext", 10).clickExpectingCondition(pageChanged);
		} catch (Exception e) {
			LOG.error("Could not click on NEXT Button.");
		}
		return this;
	}

	public GmailPage clickOnNextPassword() {
		try {
			ExpectedCondition<WebElement> pageChanged = ExpectedConditions
					.visibilityOfElementLocated(By.xpath(INBOX_LINK));
			find(Locator.ID, "passwordNext", 10).clickExpectingCondition(pageChanged);
		} catch (Exception e) {
			LOG.error("Could not click on NEXT Button.");
		}
		return this;
	}

	public GmailPage sendPassword(String password) {
		try {
			find(Locator.NAME, "password", 10).sendKeys(password);
		} catch (Exception e) {
			LOG.error("Driver issue when try to set password");
		}
		return this;
	}

	public boolean isOnMailBox() {
		try {
			find(Locator.XPATH, INBOX_LINK, 20);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean isSuccessfullMessageVisible() {
		boolean response = true;
		try {
			find(Locator.XPATH, "//*[contains(text(), 'Message sent')]", 10);
		} catch (Exception e) {
			response = false;
		}
		return response;
	}

}
