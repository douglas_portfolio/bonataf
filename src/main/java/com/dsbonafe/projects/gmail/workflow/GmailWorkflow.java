package com.dsbonafe.projects.gmail.workflow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dsbonafe.bonataf.config.selenium.structure.workflow.Workflow;
import com.dsbonafe.projects.gmail.page.GmailPage;
import com.dsbonafe.projects.gmail.page.InboxPage;

@Component
public class GmailWorkflow extends Workflow{
	
	@Autowired
	public GmailPage loginPage;
	
	@Autowired
	public InboxPage inboxPage;
	
	
	public boolean login(String username, String password){
		return loginPage.sendLogin(username)
			.clickOnNextLogin()
			.sendPassword(password)
			.clickOnNextPassword()
			.isOnMailBox();
	}


	public void composeEmail(String to, String cc, String emailSubject, String emailBody, String label) {
		inboxPage.clickOnCompose()
				 .sendEmailSubject(emailSubject)
				 .sendEmailBody(emailBody)
				 .sendTo(to)
				 .clickOnMailSettings()
				 .clickOnSettingLabel()
				 .searchForLabel(label)
				 .clickOnSend();
	}


	public boolean isEmailInTab(String tabName, String emailSubject) {
		return inboxPage.goToTab(tabName)
				 .searchEmail(emailSubject)
				 .isEmailPresent(emailSubject);
	}


	public boolean emailContaisRightBody(String body) {
		return inboxPage.getContentOfBody().contains(body);
	}
}
