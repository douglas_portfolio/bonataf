package com.dsbonafe.projects.gmail.step;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.dsbonafe.bonataf.config.selenium.StepsContext;
import com.dsbonafe.bonataf.config.selenium.structure.steps.Step;
import com.dsbonafe.projects.gmail.page.GmailPage;
import com.dsbonafe.projects.gmail.page.InboxPage;
import com.dsbonafe.projects.gmail.workflow.GmailWorkflow;

@Component
public class GmailSteps extends Step {

	@Value("${gmail.username}")
	String username;

	@Value("${gmail.password}")
	String password;

	@Value("${email.to}")
	String emailTo;

	@Value("${email.cc}")
	String emailCc;

	@Autowired
	GmailPage gmailPage;
	
	@Autowired
	InboxPage inboxPage;

	@Autowired
	GmailWorkflow gmailWorkflow;
	
	@Autowired
	StepsContext context;

	@Given("I am on the Gmail Login Page")
	public void givenIAmOnTheGmailLoginPage() {
		gmailPage.go();
		Assert.assertTrue(gmailPage.isLoaded());
	}
	
	@Given("I am loged in <$email> account")
	public void givenIAmLogedInAccount(@Named("email") String email){
		Assert.assertTrue(inboxPage.isOnAccount(email));
	}
	
	@Given("I am in my MailBox")
	public void givenIAmInMyMailBox(){
		Assert.assertTrue(inboxPage.isLoaded());
	}
	
	@Given("The sent e-mail is on the mail box under <$tabName> tab")
	public void givenTheSentEmailIsOnTheMailBoxUnderSomeTab(@Named("tabName") String tabName){
		Assert.assertTrue(gmailWorkflow.isEmailInTab(tabName, (String) context.getFromContextMap("subject")));
	}
	
	/*
	 * =========================================================================
	 */
	@When("I Login to Gmail")
	public void whenILoginToGmail() {
		gmailWorkflow.login(username, password);
	}

	@When("I send an e-mail to <TO>, cc <CC> with subject <SUBJECT>, body <BODY> and label <LABEL>")
	public void whenIComposeTheEmailWithSubjectCrossoverTest(@Named("TO") String to, @Named("CC") String cc,
			@Named("SUBJECT") String subject, @Named("BODY") String body, @Named("LABEL") String label) {
		gmailWorkflow.composeEmail(to, cc, subject, body, label);
		context.addToContextMap("subject", subject);
	}
	
	@When("I Open the received email")
	public void whenIOpenTheReceivedEmail(){
		inboxPage.openEmail();
	}
	
	@When("I Mark email as starred")
	public void whenIMarkEmailAsStarred(){
		inboxPage.starUnstarEmail();
	}

	/*
	 * =========================================================================
	 */

	@Then("Gmail should show me successfull sent message")
	public void thenGmailShouldShowMeSuccessfullSentMessage() {
		Assert.assertTrue(gmailPage.isSuccessfullMessageVisible());
	}
	
	@Then("The body of the received e-mail must be <$body>")
	public void thenEmailHasTheCorrectBody(@Named("body") String body){
		Assert.assertTrue(gmailWorkflow.emailContaisRightBody(body));
	}

}
