package com.dsbonafe.projects.gmail.step;

import org.jbehave.core.annotations.Given;
import org.springframework.stereotype.Component;

import com.dsbonafe.bonataf.config.selenium.structure.steps.Step;

@Component
public class SetupSteps extends Step {
	@Given("I am ready for BDD")
	public void givenIAmReadyForBDD() {
		LOG.debug("Everything is configured. You can write your tests.");
	}
}