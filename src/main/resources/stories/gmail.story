Narrative:
As a Gmail user
I want to create an E-mail and lable it
And I want to send it to myself
So that the e-mail should be received
And must be with the proper lable

Scenario: Sending new e-mail
Given I am on the Gmail Login Page
When I Login to Gmail
When I send an e-mail to <TO>, cc <CC> with subject <SUBJECT>, body <BODY> and label <LABEL>
Then Gmail should show me successfull sent message

Examples:
| TO | CC | SUBJECT | BODY | LABEL |
| dsbonafe@gmail.com | | "XOVER TEST" | "XOVER TEST" | "SOCIAL" |

Scenario: Receiving the e-mail
Given I am loged in <dsbonafe@gmail.com> account
Given I am in my MailBox
Given The sent e-mail is on the mail box under <Social> tab
When I Mark email as starred
When I Open the received email
Then The body of the received e-mail must be <XOVER TEST>
