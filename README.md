# README

## ABOUT BONATAF

This is a Test Framework and Application didatically put in the same project structure and separated only by packages.

The Framework properly is under bonataf.config package.
The Test Automation used to example it is under projects.gmail.

It's ideal to separate those two packages in two projects extending them in pom or gradle files.
But here I didn't do it.

This project uses **JBehave** that was configured in JBehaveConfig.java, and **Selenium**.
We have implemented the Page Objects and Fluent Software Engineering Pattern.
We are using Spring Framework and Lombok too. So, all of our Pages, Steps and Workflows are Singletons (you can change it, changing the Scope of autowireing in Spring, but it's not a good idea).
We are using a Context to share results from a step to another.
And we are using a **VideoRecorder** to save our test in video.

## WORKAROUND ON SCREEN RECORDER PACKAGE

We're using [Screen Recorder Package](http://www.randelshofer.ch/monte/) to record our Screen. This is the best solution viable. So that, in some Linux Environments the Maven Repository for this package is not working. For this reason, we have included the *JAR* for this package **only**.
It's not the best solution, but it's understood to have this great feature in our framework.
Any improvements will be thank you.

## CONFIGURING THE TEST EXECUTION
There are two wais to configure your test execution: through command line or through files.
Go to src/main/resources and change the values of each property file for a new one that you want.
There are an important file you **MUST SET** a value:

```
properties/bdd.properties
```

In this one you need to set the following values:

```
gmail.username=
gmail.password=
videoRecordFolder=
```
Don't forget to set *videoRecordFoler* in **JAVA default** (For example: C:\\myfolder);

## STORY FILES

Story files are located under `src\main\resources\stories`. You can see the specification of the tests on there.

## HOW TO USE THE FRAMEWORK

You MUST **extend** any page you will create from `com.dsbonafe.bonataf.config.selenium.structure.page.Page`.
You MUST **extend** any step you will create from `com.dsbonafe.bonataf.config.selenium.structure.steps.Step`.

## TAKING SNAPSHOTS


From any page you are able to capture the screen just calling the method:

```
public String captureScreenshot(String folder, String fileName);
```

By default, we're not taking screenshots because we're already recording the screen through a video.

By default, the video will be recorded in AVI format. So, depending on your Operational System you should install a codec from your preferency to see the movie.

## EXECUTING PROJECT WITH GRADLE

> DON'T forget to set the chromeDriver path in: `src/main/resources/properties/selenium/general.properties`.

Execute, in the root path, the following command:

```
./gradlew run --info
```

## EXECUTING PROJECT WITH MAVEN

> DON'T forget to set the chromeDriver path in: `src/main/resources/properties/selenium/general.properties`.

Execute, in the root path, the following command:

```
mvn clean install
```
